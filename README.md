En clase hemos estudiado los conceptos basicos de struct y arreglo , para poner en practica los conocimientos que se les 
solicita a los y las estudiantes que esciban un programa en C que permita al usuario llenar los datos de 10 estudiantes, 
en un arreglo de tamaño 10, de tipo struct estudiante. El programa debera entonces preguntar por el carnet y nombre de 10
estudiantes (realize la anterior con la ayuda de un ciclo). Una vez realizada la estructura, el programa tendra el siguiente
comportamiento:

PC$: Que posicion de carnet desea validar?
2
PC$: Cual es el carnet del estudiante en la posicion 2?
201925317
PC$: El carnet ingresado el correcto.
201928617
PC$: El carnet ingresado no corresponde con la opcion 2



En clase hemos estudiado los distintos de listas enlazadas y su funcionamiento. En el laboratorio anterior, usted escribió un programa que permite almacenar la información de estudiantes (nombre y carnet) en una lista enlazada simple. Para este tercer laboratorio, deberá extender su programa, para que no solamente se puedan añadir estudiantes al final de la lista, sino que exista también la posibilidad de:
a) Agregar un nuevo estudiante al inicio de la lista.
b) Borrar un estudiante a partir de su posición en la lista. Recuerde liberar el espacio en
memoria reservado, una vez que no lo esté utilizando.
Para un mejor funcionamiento de su programa, se espera que implemente también:
c) Un menú con las posibles operaciones a realizar; (i) insertar un estudiante al final de la lista, (ii) insertar un estudiante al inicio de la lista, (iii) verificar el carnet de un estudiante en una posición dada, (iv) eliminar de la lista a un estudiante en una
posición dada y (v) salir del programa.



