#include <stdio.h>

struct estudiante{ // Aqui empiezo una estructura tipo estudiante, que va a tener carnet y nombre
   int carnet;
   char nombre[25];
}estudiantes[10]; // con esto reservo 10 campos, uno para cada estudiante , los cuales van a tener un nombre y un carnet

int almacenar_informacion(){ // necesito guardar la informacion de cada estudiante respondiendo las siguientes preguntas
    char nombre;
    int carnet;
    
    for(int i=0;i<10;i++){ // aqui empiezo un ciclo, la i es el indice y le voy a empezar a sumar un 1 por cada vuelta que da

      printf("Cual es el nombre del estudiante?/n");
      scanf("%s",&estudiantes[i].nombre);  // guarda el nombre en la posicion i
      printf("cual es el numero de carnet del estudiante?/n");
      scanf("%d",&estudiantes[i].carnet); // guarda el carnet en la posicion i

    
    }

    return 0;
}
    
int hacer_pregunta(){  // esta funcion es para validar y hacer unas preguntas finales
    int pos, nuevo_carnet; 
    
     printf("Que posicion quiere validar?/n");
     scanf("%d",&pos); // aqui debe recibir un entero del 1 al 10
     printf("cual es el carnet de la posicion previa?/n");
     scanf("%d",&nuevo_carnet); // aqui debo escribir el carnet que se habia guardado en la posicion que se escribio anteriormente
     
     if(nuevo_carnet!=estudiantes[pos].carnet){ // aqui valido si son iguales o no
        printf("Es incorrecto");
        return 0;
     }
     printf("Si es correcto"); 
     return 0;
     
     }  
     
     int main(){ // con esta funcion llamo a las demas funciones que necesito
        int var;
        
        var = almacenar_informacion();
        
        return hacer_pregunta();
     }
     

